// Питання 1
// Функціі потрібні для того, щоб записувати якийсь великий шматок коду у неї і викликати за потреби та не плодити купу зайвого коду.


// Питання 2
// У функцію потрібно передавати аргумент, щоб вона брала цей аргумент за основу в своїй роботі та повертала якийсь результат.


// Питання 3
// Оператор return припиняє роботу функції та повертає якесь значення. Якщо не вказати, що саме повертати, то return поверне undefined.


// Завдання
const a = +prompt ("Введіть перше число")
const b = +prompt ("Введіть друге число")
const operationSymbol = prompt ("Введіть знак операції: +, -, / або *")
function mathOperation (a, b, operationSymbol) {
    if (operationSymbol === "+") {
        return a + b
    } else if (operationSymbol === "-") {
        return a - b
    } else if (operationSymbol === "/") {
        return a / b
    }
    return a * b
}

console.log (mathOperation (a, b, operationSymbol))


